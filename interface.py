import os
import sys
from ctypes import cdll, c_double, POINTER, CDLL, c_int
from subprocess import call


# *********************
# this class 
class Interface:

    # ********************
    # class constructor, debug=True compile F90 in debug mode
    def __init__(self, debug=False):

        # compile library by just calling make with subprocess
        self.compile_library(debug)

        # where you want to put your library
        library_path = './mylib.so'

        # check if library exists
        if not os.path.exists(library_path):
            print("ERROR: library " + library_path + " not found, check for compilation errors!")
            sys.exit()

        # load f90 library compiled with fpic and shared
        self.library = cdll.LoadLibrary(library_path)

        # loaded message
        print("Library " + library_path + " loaded!")

    # *********************
    # not-so-elegant way of unlinking the library
    # from here
    # https://stackoverflow.com/questions/359498/how-can-i-unload-a-dll-using-ctypes-in-python
    #def __del__(self):
    #    def isLoaded(lib):
    #        libp = os.path.abspath(lib)
    #        ret = os.system("lsof -p %d | grep %s > /dev/null" % (os.getpid(), libp))
    #        return ret == 0
    #
    #    handle = self.library._handle
    #    name = self.library._name
    #    del self.library
    #    while isLoaded(name):
    #        libdl = CDLL("libdl.so")
    #        libdl.dlclose(handle)

    # *******************
    # compile library to obtain .so
    @staticmethod
    def compile_library(debug):
        print("Compiling f90 files...")
        if debug:
            call(["make", "clean"])
            call(["make", "debug"])
        else:
            call(["make"])

    # *********************
    # solve chemistry from initial conditions
    def do_something(self, var_array, var_double):

        # array size
        var_int = len(var_array)

        # define library function INPUT types
        self.library.do_something.argtypes = [POINTER(c_double), POINTER(c_double), POINTER(c_int)]

        # library function returns None as OUTPUT, since f90 subroutine
        self.library.do_something.restype = None

        # convert array to c_double array
        var_array_ctype = (c_double * var_int)(*var_array)  # pointer to double array

        # convert double argument to c_double
        var_double_ctype = c_double(var_double)

        # convert integer to c_int
        var_int_ctype = c_int(var_int)

        # do something
        self.library.do_something(var_array_ctype, var_double_ctype, var_int_ctype)

        # return c_double results as list
        return list(var_array_ctype)
