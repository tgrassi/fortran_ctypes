# README #

Minimum working example to call Fortran from Python using ctypes.     
Questions and comments to Tommaso Grassi (USM/LMU), tgrassi@usm.lmu.de

## Requirements ##
A Fortran compiler as gfortran or Intel fortran compiler (ifort).     
On linux simply
```
sudo apt-get install gfortran
```

## Run ##
```
python main.py
```

## Expected output ##
With ifort
```
Compiling f90 files...
ifort -O3 -xHost -unroll -g -pg -fPIC -c mylib.f90 -o mylib.o
ifort -shared mylib.o -o mylib.so -O3 -xHost -unroll -g -pg -fPIC 
Library ./mylib.so loaded!
[21.0, 30.0, 12365.6]
```
or using gfortran
```
Compiling f90 files...
gfortran -ffree-line-length-none -O3 -g -fPIC -c mylib.f90 -o mylib.o
gfortran -shared mylib.o -o mylib.so -ffree-line-length-none -O3 -g -fPIC 
Library ./mylib.so loaded!
[21.0, 30.0, 12365.6]
```

## Under the hood ##
`main.py` imports the class `Interface`, with a constructor that compiles `mylib.f90` using `Makefile` creating `mylib.so`.      
The constructor also uses `cdll.LoadLibrary` to bind the compiled library `mylib.so`.       
As example, in `mylib.f90` there is a subroutine (`do_something(array_of_double, double)`) that sums a double to each element of an array, like     
```
f(A(:), B) = A(:) + B
```
You can call this subroutine directly from `main.py`     
```
from interface import Interface
a = Interface()
result = a.do_something(my_array, my_float)
```

### Python ###
The tricky part is in `do_something` method in `interface.py` where the ctype conversion is made.      
In particular, you have to      

* define the types of the input arguments, `argtypes`.
* define the type of output (None, since a fortran subroutine corresponds to a void type C function), `restype`.
* convert the input variables to ctypes variables, `c_int`, `c_double`, ...

### Fortran ###
Fortran needs `use iso_c_binding`, `bind(C)`, to and define input/output variables with ctypes (see `mylib.f90`).      
Note that you need to add `-fPIC` and `-shared` in your `Makefile` to create `mylib.so`.     
`Makefile` looks for `ifort`, if not found uses `gfortran`.      


## Unbinding the library ##
There are known problems with `cdll.LoadLibrary`, so that once your library is loaded, it cannot be replaced with a recompiled one.       
This issue is discussed [here](https://stackoverflow.com/questions/359498/how-can-i-unload-a-dll-using-ctypes-in-python), and it is not-so-elegantly implemented in `__del__` in `interface.py`.
