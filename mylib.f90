module mylib
contains

  subroutine do_something(var_array, var_double, var_integer) bind(C)
    use iso_c_binding
    implicit none
    integer(C_INT),intent(in)::var_integer
    real(C_DOUBLE),intent(inout)::var_array(var_integer)
    real(C_DOUBLE),intent(in)::var_double

    var_array(:) = var_array(:) + var_double

  end subroutine do_something

end module mylib
